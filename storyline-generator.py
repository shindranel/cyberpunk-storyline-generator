#!/usr/bin/env python3

from random import randrange

#list hell
hooks_list = ['Coronet Blue', 'Crisis', 'Discovery', 'False Accusation', 'Kidnapped',
              'Looming Threat', 'Murder', 'Revelation', 'Cliffhanger', 'Development']
cliffhangers_list = ['Ambush', 'Battle', 'Chase', 'Confrontation', 'Contest',
                     'Dogfight', 'Duel', 'Fist Fight', 'Monster', 'Obstacles', 'Pursuit',
                     'Race', 'Skirmish!']
dev_list = ['Advance Revealed', 'Alliance', 'Back from the Dead', 'Betrayal', 'Clue',
            'Foreshadowing', 'Framed!', 'Gain Mastery', 'Hazardous Quest', 'Hesitation',
            'Lie Revealed', 'Mistaken Identity', 'Monologue', 'Not What it Seems',
            'Obsession', 'Personal Stake', 'Puzzle', 'Rescuers!', 'Retreat',
            'Revealed Weakness', 'Revelation', 'Romance', 'Sabotage', 'Second Chance',
            'Secret Meeting', 'Strange Bedfellows', 'Turnabout!', 'Vengeance!', 'Warning']
climax_list = ['Final Battle', 'Final Revelation', 'Final Act']
resolution_list = ['Antagonist Escapes', 'Antagonist is Killed', 'Antagonist Toppled',
                   'Edgerunners Captured', 'Edgerunners Escape', 'Ending Cliffhanger',
                   'Greater Threat', 'Happy Ending', 'Pyrrhic Victory']
battles_list = ['Mooks, one for every Edgerunner', 'Lieutenants, one for two Edgerunners',
                'Mini Bosses, one for three Edgerunners', 'Final Boss']

###Functions and classes

def define_battle():
        global battle_number
        adversity = battles_list[battle_number]
        battle_number += 1
        if (battle_number > 3):
                battle_number = 3
        return(adversity)
        
def define_dev():
        i = randrange(len(dev_list))
        return(dev_list[i])

def define_cliffhanger():
        i = randrange(len(cliffhangers_list))
        if (i == 1):
                return('Battle: ' + define_battle())
        return(cliffhangers_list[i])

def define_hook():
        i = randrange(10)
        if (i == 8):
                return(define_cliffhanger())
        elif (i == 9):
                return(define_dev())
        return(hooks_list[i])

def fill_list(liste):
        trigger = 0
        i = 0
        while (i < len(liste)):
                if (liste[i] == ''):
                        if (not trigger):
                                liste[i] = define_dev()
                                trigger = 1
                        else:
                                liste[i] = define_cliffhanger()
                                trigger = 0
                i += 1
        return(liste)
        

def imperative_beats(liste):
        if (len(liste) < 3):
                print('Too short for a real scenario, do it yourself')
                return()
        liste[0] = define_hook()
        liste[len(liste) - 1] = resolution_list[randrange(9)]        
        liste[len(liste) - 2] = climax_list[randrange(3)]
        return(liste)

def create_list(n):
        new_list = []
        i = 0
        while (i < n):
                new_list.append('')
                i += 1
        new_list = imperative_beats(new_list)
        new_list = fill_list(new_list)
        return(new_list)

def print_scenario(beat_list):
        i = 1
        trigger = 0
        print('Your scenario is:\n')
        print('Hook:          ', beat_list[0])
        while (i < (len(beat_list) - 2)):
                if (not trigger):
                        print('Development:   ', beat_list[i])
                        trigger = 1
                else:
                        print('Cliffhanger:   ', beat_list[i])
                        trigger = 0
                i += 1
        print('Climax:        ', beat_list[len(beat_list) - 2])
        print('Resolution:    ', beat_list[len(beat_list) - 1])

###Variables

reminder = 0
battle_number = 0

### Core segment
print('Welcome to the Storyline generator!\n\nPlease indicate how long you want your game to be, in hours:')
test = input()
beat_number = (float(test) * 2)
beat_number = int(beat_number)
if (beat_number < 3):
        beat_number = 3
        print("\nNot even two hours? Shit, can\'t help you there choomba. \nI\'ll make you a basic story, \
with a hook, a climax and a resolution. Best I can do.")
beat_list = create_list(beat_number)

### Print
print('You want to play', test, 'hours. Okay.\nIt means playing', int(beat_number),'beats.\n')
print_scenario(beat_list)
